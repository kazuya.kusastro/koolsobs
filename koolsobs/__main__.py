#!/usr/bin/env python3

### __main__.py: the main program of triccsred

### import modules
import argparse
import os
import subprocess
import sys

### define constants
VERSIONS = ('stable', 'develop', 'old_stable')
KOOLS_HOST_IP = '192.168.1.64'

### define functions
def main(args):
# overwrite GUI and server version
    if args.develop:
        args.gui_version = args.server_version = 'develop'
    elif args.old_version:
        args.gui_version = args.server_version = 'old_stable'

# check whether the host is the KOOLS-IFU host PC
    ret = subprocess.run(['hostname', '--all-ip-addresses'], stdout=subprocess.PIPE)
    host_ip_list = ret.stdout.decode().split()

# set koolsobs and koolscontrol directories
    if KOOLS_HOST_IP in host_ip_list:
        sys.path.insert(0, '/home/messia/kools/{}/koolsobs'.format(args.gui_version))

        if args.reboot_server:
            path_server = '/home/messia/kools/{}/koolscontrol/koolscontrol/__main__.py'.format(args.server_version)
            if args.debug:
                subprocess.Popen(['python3', path_server, 'restart'])
            else:
                subprocess.Popen(['python3', path_server, 'restart'], stdout=subprocess.DEVNULL)

    import koolsgui
    koolsgui.main_window.show(args)

    return

if __name__ == '__main__':
# argparse
    parser = argparse.ArgumentParser(description='KOOLS-IFU observation GUI')
    parser.add_argument('-d', '--develop', action='store_true', help='start GUI and servers of the develop version')
    parser.add_argument('--old-version', action='store_true', help='start GUI and servers of the old version')
    parser.add_argument('-e', '--expert', action='store_true', help='GUI for experts')
    parser.add_argument('-g', '--socket-global', action='store_true', help='launch GUI from other PCs')
    parser.add_argument('-r', '--reboot-server', action='store_true', help='reboot servers')
    parser.add_argument('--uv_ha', action='store_true', help='add UV and Ha modes for VPH-blue')
    parser.add_argument('--no_wipe', action='store_true', help='enable no wipe mode')
    parser.add_argument('--socket-disable', action='store_true', help='disable socket connection')
    parser.add_argument('--host', help='host for socket connection')
    parser.add_argument('--port', type=int, help='port for socket connection')
    parser.add_argument('--gui-version', choices=VERSIONS, default='stable', help='GUI version')
    parser.add_argument('--server-version', choices=VERSIONS, default='stable', help='server version')
    parser.add_argument('--debug', action='store_true', help='debug mode in restarting the status server')


    main(parser.parse_args())

