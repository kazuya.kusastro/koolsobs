#!/usr/bin/python3
# coding: utf-8

### kools_obs_gui.py: KOOLS-IFU status/control GUI ###

### import libraries
import argparse
import os
import subprocess
import sys
import time

from PyQt5.QtWidgets import QApplication

# KOOLS-IFU observation modules
import koolsobs

### functions
def set_font_small():
    koolsobs.const_var.fontLabel.setPixelSize(24)
    koolsobs.const_var.font.setPixelSize(16)
    koolsobs.const_var.fontToolTip.setPixelSize(14)

### main function
if __name__ == '__main__':
# set argparse
    parser = argparse.ArgumentParser(description='Launch KOOLS-IFU control GUI')
    parser.add_argument('-d', '--develop', help='Launch server in develop directory', action='store_true')
    parser.add_argument('-e', '--gui_expert', help='GUI for experts', action='store_true')
    parser.add_argument('-g', '--soc_global', help='Launch GUI from other PCs', action='store_true')
    parser.add_argument('-r', '--reboot_server', help='Reboot KOOLS servers', action='store_true')
    parser.add_argument('--socket-disable', help='Disable socket connection', action='store_true')
    parser.add_argument('--uv_ha', help='Add UV and Ha modes for VPH-blue', action='store_true')
    parser.add_argument('--no_wipe', help='Enable no wipe mode', action='store_true')
    parser.add_argument('--fiberbundle_id', choices=['2014', '2019'], help='Set fiberbundle ID')

    args = parser.parse_args()

# include argparse settings
    if args.develop:
        kools_server_dir = koolsobs.const_var.kools_develop_dir
    else:
        kools_server_dir = koolsobs.const_var.kools_server_dir

    if args.gui_expert:
        koolsobs.const_var.gui_expert = True
        set_font_small()

    if args.soc_global:
        koolsobs.const_var.socket_global = True

    if args.reboot_server:
        ret = subprocess.run(['hostname', '--all-ip-addresses'], stdout=subprocess.PIPE)
        ret_split = ret.stdout.decode().split()
        if koolsobs.const_var.host in ret_split:
            print('Reboot KOOLS servers.')
            subprocess.run('{}start_stop_server.sh restart'.format(kools_server_dir).split(), check=True)
            time.sleep(1)

# remove the lock files
            lock_file_path = koolsobs.const_var.kools_const_dir + '.read_ccd.lock'
            if os.path.isfile(lock_file_path):
                os.remove(lock_file_path)

    if args.socket_disable:
        koolsobs.const_var.socket_disable = True
    else:
# sounds turned on in default
        koolsobs.common_func.socket_connect(['set_status sound_exp On'])

    if args.uv_ha:
        koolsobs.const_var.vph_blue_uv_ha = True

    if args.no_wipe:
        koolsobs.const_var.no_wipe = True

# set fiberbundle ID
    if args.fiberbundle_id:
        koolsobs.common_func.socket_connect(['set_status fiberbundle_id {}'.format(args.fiberbundle_id)])

# start GUI
    app = QApplication(sys.argv)
#  main_window = koolsobs.MainWindow.MainWindow()
    main_window = koolsobs.menubar.MenuBar()

# show main window
    main_window.show()
#  main_window_menu.show()
# exit main function if exit called
    sys.exit(app.exec_())
