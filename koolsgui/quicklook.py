#!/usr/bin/python3

### gui/quicklook.py: GUI for the QuickLook window

### import libraries
import os
import subprocess

from PyQt5.QtWidgets import (QGridLayout, QLabel, QLineEdit, QFrame, QGraphicsView, QGraphicsScene, QGraphicsPixmapItem)
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt

# original libraries
from const_var import const_var

### set constants

## define observation log box
class Frame(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# define constant
        self.ql_frame_now = 0

        self.label = {}
        self.inputbox = {}

# add widgets
        self.label['image'] = QLabel('Image')
        self.viewImage = QGraphicsView()
        self.sceneImage = QGraphicsScene(self)
        self.pixmapImage = QPixmap()
        self.viewImage.setScene(self.sceneImage)
        self.viewImage.resize(30, 20)

        self.label['spec'] = QLabel('Spectrum')
#        self.label['spec'] = QLabel('Spectrum                               ')
        self.viewSpec = QGraphicsView()
        self.sceneSpec = QGraphicsScene(self)
        self.pixmapSpec = QPixmap()
        self.viewSpec.setScene(self.sceneSpec)

#        self.pixmapImage.load('/home/kazuya/data/fiber-ifs/software/koolsobs/koolsobs/gui/200x200.png')
#        print(const_var['module_dir'])
        self.pixmapImage.load(const_var['module_dir'] + '/figure/200x157.png')
        self.pixmapSpec.load(const_var['module_dir'] + '/figure/600x220.png')
        self.sceneImage.addItem(QGraphicsPixmapItem(self.pixmapImage))
        self.sceneSpec.addItem(QGraphicsPixmapItem(self.pixmapSpec))

        self.label['frame_id'] = QLabel('Frame ID')
        self.label['frame_id'].setAlignment(Qt.AlignRight)
        self.inputbox['frame_id'] = QLineEdit()
        self.inputbox['frame_id'].setFixedWidth(150)
        self.inputbox['frame_id'].setAlignment(Qt.AlignCenter)
        self.inputbox['frame_id'].setText(str(self.ql_frame_now).zfill(8))

# set the common settings
        [self.label[i].setFont(const_var['font']) for i in self.label]
        [self.inputbox[i].setFont(const_var['font']) for i in self.inputbox]
        [self.inputbox[i].setReadOnly(True) for i in self.inputbox]
        [self.inputbox[i].setStyleSheet('QWidget { border: 2px solid black }') for i in self.inputbox]

# layout
        layout = QGridLayout()
        layout.setSpacing(10)

        layout.addWidget(self.label['image'], 0, 0)
        layout.addWidget(self.viewImage, 1, 0)
        layout.addWidget(self.label['spec'], 0, 1)
        layout.addWidget(self.viewSpec, 1, 1, 1, 3)

        layout.addWidget(self.label['frame_id'], 0, 2)
        layout.addWidget(self.inputbox['frame_id'], 0, 3)

        self.setLayout(layout)

# functions
    def openImages(self):
# open the observed image and spectrum
        frame_num_str = const_var['ql_latest'].zfill(8)
        file_dir = '/home/messia/quicklook/'

# open the quicklook image and spectrum if exists
        if int(const_var['ql_latest']) > int(self.ql_frame_now):
            file_image = file_dir + 'kif{}-sky-image.png'.format(frame_num_str)
            file_spec = file_dir + 'kif{}-sky-comb.png'.format(frame_num_str)
            file_no_grism = file_dir + 'kif{}-bias-image.png'.format(frame_num_str)

            if os.path.isfile(file_image) and os.path.isfile(file_spec):
                self.pixmapImage.load(file_image)
                self.pixmapSpec.load(file_spec)

            elif os.path.isfile(file_no_grism):
                self.pixmapImage.load(file_no_grism)
                self.pixmapSpec.load('600x220.png')

# set the default images
            else:
                self.pixmapImage.load('200x200.png')
                self.pixmapSpec.load('600x220.png')

            self.sceneImage.addItem(QGraphicsPixmapItem(self.pixmapImage))
            self.sceneSpec.addItem(QGraphicsPixmapItem(self.pixmapSpec))

# update ql_frame_now
            self.ql_frame_now = const_var['ql_latest']
            self.inputbox['frame_id'].setText(self.ql_frame_now.zfill(8))

    def update_boxes(self):
        self.openImages()
