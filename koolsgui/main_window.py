#!/usr/bin/python3
# coding: utf-8

### main_window.py: define the main window

### import libraries
import subprocess
import sys
import time

#import PyQt5
from PyQt5.QtWidgets import (QMainWindow, QWidget, QVBoxLayout, QTabWidget, QFrame, QSplitter, QMessageBox, QAction, QApplication, qApp)
from PyQt5.QtGui import (QFont, QIcon)
from PyQt5.QtCore import (Qt, QTimer)

# original libralies
from const_var import const_var
import common_func, menubar, obs_command, motor, exposure, status, job, log, quicklook

### set constants

## define main window class
class MainWindow(QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

# initialize
        self.frame = {}

# make buttons and input/output boxes
        if const_var['gui_expert']:
            self.frame['motor'] = motor.Frame(self)
            self.frame['motor'].setFrameShape(QFrame.Panel)
            self.frame['exposure'] = exposure.Frame(self)
            self.frame['exposure'].setFrameShape(QFrame.Panel)
        else:
            self.frame['obs_command'] = obs_command.Frame(self)
            self.frame['obs_command'].setFrameShape(QFrame.Panel)

        self.frame['status'] = status.Frame(self)
        self.frame['status'].setFrameShape(QFrame.Panel)

        self.frame['job'] = job.Frame(self)
        self.frame['log'] = log.Frame(self)
        self.frame['quicklook'] = quicklook.Frame(self)

        tabs = QTabWidget()
        tabs.addTab(self.frame['job'], 'Job')
        tabs.addTab(self.frame['log'], 'Log')
        tabs.addTab(self.frame['quicklook'], 'Quick Look')
        tabs.setFont(const_var['font'])

# layout
        self.layout = QVBoxLayout()

        splitter = QSplitter(Qt.Horizontal)
#    splitter.addWidget(self.frameMessia)
        if const_var['gui_expert']:
            splitter.addWidget(self.frame['motor'])
            splitter.addWidget(self.frame['exposure'])
        else:
            splitter.addWidget(self.frame['obs_command'])
        splitter.addWidget(self.frame['status'])
        splitter.setHandleWidth(10)

        self.layout.addWidget(splitter)
        self.layout.addWidget(tabs)

        self.setLayout(self.layout)
#    self.resize(700, 300)
        self.setWindowTitle('KOOLS-IFU GUI')

# get status every 0.5 second
        self.timer = QTimer()
        self.timer.setInterval(const_var['gui_interval'])
        self.timer.timeout.connect(self.update_status)
        self.timer.start()

## function
    def check_signal_tel_lamp(self):
        if const_var['signal_exp'] == 'green':
            return

        if const_var['signal_tel'] == 'red':
            title = 'Confrim stage position'
            message = 'The stage position is not proper. Are you sure you want to proceed anyway?'
        elif const_var['signal_lamp'] == 'red':
            title = 'No lamp turned on'
            message = 'The lamp is not turned on. Are you sure you want to proceed anyway?'
        else:
            return

        msg_box = QMessageBox()
        msg_box.setIcon(QMessageBox.Question)
        msg_box.setWindowTitle(title)
        msg_box.setText(message)
        msg_box.setFont(const_var.fontToolTip)
        msg_box.setStandardButtons(QMessageBox.Yes | QMessageBox.Retry | QMessageBox.No)
        msg_box.setDefaultButton(QMessageBox.No)
        reply = msg_box.exec()

        if reply == QMessageBox.Yes:
            ret = common_func.socket_connect(['get_job 1st'])
            common_func.socket_connect(['set_status signal_tel green', 'set_status signal_lamp green', 'delete_job 1', 'insert_job 1 {} No_check'.format(ret[0]), 'set_status status_job start'])
        elif reply == QMessageBox.Retry:
            common_func.socket_connect(['set_status signal_tel green', 'set_status signal_lamp green', 'set_status status_job start'])
        else:
            common_func.socket_connect(['set_status signal_tel green', 'set_status signal_lamp green'])

        return

    def change_interval(self):
        self.timer.setInterval(const_var['gui_interval'])
        self.timer.start()

        return

    def update_status(self):
        if const_var['gui_expert']:
            self.update_status_expert()
        else:
            self.update_status_simple()

        return

    def update_status_simple(self):
        if const_var['socket_disable']:
            self.frame['obs_command'].textbox.setText('socket disable')
        else:
            common_func.get_kools_status()
            self.check_signal_tel_lamp()
            self.frame['obs_command'].update_boxes()
            self.frame['status'].update_boxes()
            self.frame['job'].update_boxes()
            self.frame['quicklook'].update_boxes()

        return

    def update_status_expert(self):
        if const_var['socket_disable']:
            self.frame['exposure'].textbox.setText('socket disable')
        else:
            common_func.get_kools_status()
            self.check_signal_tel_lamp()
            self.frame['motor'].update_boxes()
            self.frame['exposure'].update_boxes()
            self.frame['status'].update_boxes()
            self.frame['job'].update_boxes()
            self.frame['quicklook'].update_boxes()

        return

# define functions
def set_font_small():
    const_var['font_large'].setPixelSize(24)
    const_var['font'].setPixelSize(16)
    const_var['font_tooltip'].setPixelSize(14)

    return

def show(args):
# include argparse settings
    if args.expert:
        const_var['gui_expert'] = True
        set_font_small()

    if args.uv_ha:
        const_var['vph_blue_uv_ha'] = True

    if args.no_wipe:
        const_var['no_wipe'] = True

    if args.socket_global:
        const_var['socket_global'] = True

    if hasattr(args, 'host'):
        const_var['socket_host'] = args.host

    if hasattr(args, 'port'):
        const_var['socket_port'] = args.port

    ret = subprocess.run(['hostname', '--all-ip-addresses'], stdout=subprocess.PIPE)
    ip_list = ret.stdout.decode().split()

    if args.socket_disable:
        const_var['socket_disable'] = True
    else:
# sounds turned on in default
        common_func.socket_connect(['set_status sound_exp On'])

# set the window title
    window_title = 'KOOLS-IFU observation GUI'
    if args.gui_version != 'stable':
        window_title += ' ({})'.format(args.gui_version)

# start GUI
    app = QApplication(sys.argv)
    main_window = menubar.MenuBar()
    main_window.setWindowTitle(window_title)

# show main window
    main_window.show()
# exit main function if exit called
    sys.exit(app.exec_())

