#!/usr/bin/python3
# coding: utf-8

### const_var.py: define constants and variables

### import libraries
import os

from PyQt5.QtGui import QFont

### set constants and variables
const_var = {}

## test
const_var['server_version'] = 'nop'

## constants
const_var['motor_name'] = ('Wheel-A', 'Wheel-B', 'Grism', 'Camera')
const_var['module_dir'] = os.path.dirname(os.path.abspath(__file__))

# socket connection
const_var['socket_host'] = '192.168.1.64'
const_var['socket_port'] = 50007

## variables
# font
const_var['font_large'] = QFont()
const_var['font_large'].setPixelSize(28)
const_var['font'] = QFont()
const_var['font'].setPixelSize(20)
const_var['font_tooltip'] = QFont()
const_var['font_tooltip'].setPixelSize(16)

# GUI
const_var['gui_expert'] = False
const_var['gui_interval'] = 500

const_var['socket_disable'] = False
const_var['socket_global'] = False

const_var['init_grism_list'] = False
const_var['init_motor_list'] = False

## KOOLS status
# messia
const_var['status_messia'] = 'stop'
const_var['message_messia'] = ''
const_var['temp_ccd_now'] = 'nop'
const_var['temp_heater_now'] = '0'

# motor
const_var['message_motor'] = ''
for tmp_motor in const_var['motor_name']:
    const_var['pos_' + tmp_motor] = 'unknown'
    const_var['pulse_' + tmp_motor] = 'unknown'

# quicklook
const_var['ql_latest'] = '0'

# sound at the exposure end
const_var['sound_exp'] = 'Off'

# special observation mode
const_var['vph_blue_uv_ha'] = False
const_var['no_wipe'] = False

## telescope
const_var['signal_tel'] = 'green'
const_var['signal_lamp'] = 'green'
const_var['signal_exp'] = 'green'

# exposure
const_var['local_exp_str'] = '00:00:00'
const_var['ccd_file_number'] = 41148
const_var['message_exp'] = ''

# job
const_var['status_job'] = 'stop'
const_var['append_insert_job'] = 'Append'
const_var['job_now'] = 'finish'
const_var['job_list'] = []
const_var['job_current_id'] = 0

# misc
const_var['status_shutter'] = 'nop'
