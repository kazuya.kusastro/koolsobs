#!/usr/bin/python3
# coding: utf-8

### menubar.py: define the menubar

### import libraries
from PyQt5.QtWidgets import (QMainWindow, QWidget, QVBoxLayout, QTabWidget, QFrame, QSplitter, QMessageBox, QAction, QMenu, qApp)
from PyQt5.QtGui import (QFont, QIcon)
from PyQt5.QtCore import (Qt, QTimer)

# original libraries
from const_var import const_var
import common_func, main_window

### set constants

## define main window class
class MenuBar(QMainWindow):
    def __init__(self, parent=None):
        super(MenuBar, self).__init__(parent)

## init MainWindow
        self.main_window = main_window.MainWindow()
        self.setCentralWidget(self.main_window)

## set variables
        menubar = self.menuBar()
        menu = []
        content = []

## define actions
# file
        menu.append(QMenu('&File', self))
        content.append([])

        content[-1].append(QAction('&Reload grism list', self))
        content[-1][-1].triggered.connect(self.update_motor_list)

        content[-1].append(QAction('&Exit', self))
        content[-1][-1].setShortcut('Ctrl+Q')
        content[-1][-1].triggered.connect(self.closeEvent)

# setting
        menu.append(QMenu('&Setting', self))
        content.append([])

        content[-1].append(QMenu('GUI status update interval', self))
        interval = []
        for count_interval, tmp_interval in enumerate((0.2, 0.5, 1.0, 2.0)):
            tmp_action = QAction('{} s'.format(tmp_interval), self)
            tmp_action.triggered.connect(self.set_interval)
            content[-1][-1].addAction(tmp_action)

# develop
        menu.append(QMenu('&Develop', self))
        content.append([])

        content[-1].append(QAction('Show server version', self))
        content[-1][-1].triggered.connect(self.show_server_version)

# add menu
        for count_menu in range(len(menu)):
            for tmp_content in content[count_menu]:
                if type(tmp_content) is QAction:
                    menu[count_menu].addAction(tmp_content)
                elif type(tmp_content) is QMenu:
                    menu[count_menu].addMenu(tmp_content)
            menubar.addMenu(menu[count_menu])

### define functions
    def update_motor_list(self):
        if const_var['gui_expert']:
            self.main_window.frame['motor'].update_motor_list()
            self.main_window.frame['exposure'].update_grism_list()
        else:
            self.main_window.frame['obs_command'].update_grism_list()

        return

    def closeEvent(self, event):
        msg_box = QMessageBox()
        msg_box.setIcon(QMessageBox.Question)
        msg_box.setWindowTitle('Quit GUI')
        msg_box.setText('Do you want to close this GUI?')
        msg_box.setFont(const_var['font_tooltip'])
        msg_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        msg_box.setDefaultButton(QMessageBox.No)
        reply = msg_box.exec()

#    reply = QMessageBox.question(self, 'Quit GUI', 'Do you want to close this GUI?', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            qApp.quit()
        else:
            try:
                event.ignore()
            except:
                pass

    def set_interval(self, input_time):
        const_var['gui_interval'] = int(input_time)
        self.main_window.change_interval()
        return

    def show_server_version(self):
        print('Server version: ' + const_var['server_version'])
        return


