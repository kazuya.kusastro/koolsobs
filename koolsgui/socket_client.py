#!/usr/bin/python3
# coding: utf-8

### import libraries
import argparse
import datetime
import os
import socket
import sys

# original libraries
from const_var import const_var

### set constants
socket_local_file = '/home/messia/kools/const_file/status_local.sock'

#host = '192.168.1.64'
#port = 50007
delimiter = '!DELIMITER!'
message_end = '!MESSAGE_END!'

### define functions
def socket_send_recv(soc, message_list):
# make send command
    send_cmd = (delimiter).join(message_list) + message_end
    soc.send(send_cmd.encode())

    ret = ''
    while True:
        tmp_ret = soc.recv(4096).decode()
        if tmp_ret == '':
            break
        ret += tmp_ret
        if ret.endswith(message_end):
            break

    soc.shutdown(socket.SHUT_RDWR)
    soc.close()

    return ret.removesuffix(message_end).split(delimiter)

def socket_global(message_list):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as soc:
        soc.settimeout(3)
        try:
            soc.connect((const_var['socket_host'], const_var['socket_port']))
        except socket.error:
            return ['socket connection error']

        return socket_send_recv(soc, message_list)

def socket_local(message_list):
    with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as soc:
        soc.settimeout(3)
        try:
#            soc.connect(const_var.socket_local_file)
            soc.connect(socket_local_file)
        except socket.error:
            return ['socket connection error']

        return socket_send_recv(soc, message_list)

### main function
def main():
# argparse
    parser = argparse.ArgumentParser(description='Client process for socket connection with KOOLS')
#  parser = argparse.ArgumentParser(description = 'Client process for socket connection with KOOLS.', add_help = False, formatter_class = argparse.RawTextHelpFormatter)

    parser.add_argument('command_main', choices=['quit', 'quit_now', 'quick_command', 'get_status', 'set_status', 'get_job', 'set_job', 'append_job', 'insert_job', 'delete_job'], nargs=1, help='Main command for server')
    parser.add_argument('command_sub', nargs='*', help='Command arguments for server.')
#  parser.add_argument('--help', action='store_true', help='Show this help message and exit.')
    parser.add_argument('-g', '--soc_global', action='store_true', help='Use global socket connection')
    parser.add_argument('--time_stamp', action='store_true', help='Display time stamp.')
    parser.add_argument('--no_update_flag', action='store_true', help='Do not activate the update flag')

    args = parser.parse_args()

# add update flag
    if args.no_update_flag:
        args.command_sub.append('no_update_flag')

# send the message
    if args.soc_global:
        ret = socket_global([' '.join([args.command_main[0]] + args.command_sub)])
    else:
        ret = socket_local([' '.join([args.command_main[0]] + args.command_sub)])

#  ret = send_message([args.command_main[0]] + args.command_sub)

# output time
    if args.time_stamp:
        print(datetime.datetime.now())

# output messages
    [print(i) for i in ret]
#  for ret_line in ret:
#    print(ret_line)

### main
if __name__ == '__main__':
    sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))
    main()
