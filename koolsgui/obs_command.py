#!/usr/bin/python3
# coding: utf-8

### gui/ObsCommand.py: GUI for observation commands ###

### import libraries
import subprocess
import time

from PyQt5.QtWidgets import (QGridLayout, QLabel, QLineEdit, QTextEdit, QPushButton, QComboBox, QCheckBox, QFrame)
from PyQt5.QtCore import (Qt, QSize)

# original libraries
import common_func
from const_var import const_var

### set constants

## define ObsCommand box class
class Frame(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
        self.label = {}
        self.inputbox = {}
        self.button = {}
        self.combobox = {}

# set variables
        self.nameFilterGrism = []

# make buttons and input/output boxes
        label_obs_command = QLabel('Obs. commands')
        label_obs_command.setFont(const_var['font_large'])

        self.label['messia'] = QLabel('MESSIA')
        self.button['start_messia'] = QPushButton('Start')
        self.button['start_messia'].clicked.connect(self.startMessia)
        self.button['start_messia'].setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button['stop_messia'] = QPushButton('Stop')
        self.button['stop_messia'].clicked.connect(self.stopMessia)
        self.button['stop_messia'].setStyleSheet('QWidget { background-color: #FFB0B0 }')

        self.button['init_all_motor'] = QPushButton('Initialize all motors')
        self.button['init_all_motor'].setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button['init_all_motor'].clicked.connect(self.initAllMotor)

        self.label['filter_grism'] = QLabel('Filter and Grism')
        self.combobox['filter_grism'] = QComboBox()

        self.label['exp_time'] = QLabel('Exposure time')
        self.inputbox['exp_time'] = QLineEdit()
        self.inputbox['exp_time'].setText('')
        self.inputbox['exp_time'].setPlaceholderText('Input Exp. Time')
        self.inputbox['exp_time'].setMaxLength(5)
        self.label['exp_unit'] = QLabel('sec.')

        self.label['exp_multi'] = QLabel('Multiple exposure')
        self.inputbox['exp_multi'] = QLineEdit()
        self.inputbox['exp_multi'].setText('1')
        self.inputbox['exp_multi'].setPlaceholderText('Input Number of Exp.')
        self.inputbox['exp_multi'].setMaxLength(4)
        self.label['times'] = QLabel('times')

        self.label['no_wipe'] = QLabel('No wipe mode')
        self.button['no_wipe'] = QPushButton()
        self.button['no_wipe'].setCheckable(True)
        self.button['no_wipe'].setText('Off')
        self.button['no_wipe'].toggled.connect(self.toggle_no_wipe)

        self.button['start_exp_sequence'] = QPushButton('Start exposure sequence')
        self.button['start_exp_sequence'].setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button['start_exp_sequence'].clicked.connect(self.startExposureSequence)

        self.button['stop_all'] = QPushButton('Stop Motors and Exposure')
        self.button['stop_all'].clicked.connect(self.stopMotorExposure)
        self.button['stop_all'].setStyleSheet('QWidget { background-color: #FF0000 }')

        self.label['message'] = QLabel('Message')
        self.textbox = QTextEdit()
        self.textbox.setFont(const_var['font'])
        self.textbox.setReadOnly(True)
        self.textbox.setStyleSheet('QWidget { border: 2px solid black }')

# set the common settings
        [self.label[i].setFont(const_var['font']) for i in self.label]
        [self.combobox[i].setFont(const_var['font']) for i in self.combobox]
        [self.button[i].setFont(const_var['font']) for i in self.button]
        [self.inputbox[i].setFont(const_var['font']) for i in self.inputbox]
#        [self.inputbox[i].setReadOnly(True) for i in self.inputbox]
        [self.inputbox[i].setAlignment(Qt.AlignCenter) for i in self.inputbox]
#        [self.radio[i].setFont(const_var['font']) for i in self.radio]

# layout
        layout = QGridLayout()
        layout.setSpacing(10)
        count_row = 0

        layout.addWidget(label_obs_command, count_row, 0)
        count_row += 1

        layout.addWidget(self.label['messia'], count_row, 0)
        layout.addWidget(self.button['start_messia'], count_row, 1)
        layout.addWidget(self.button['stop_messia'], count_row, 2)
        count_row += 1

        layout.addWidget(self.button['init_all_motor'], count_row, 0, 1, 3)
        count_row += 1

        layout.addWidget(self.label['filter_grism'], count_row, 0)
        layout.addWidget(self.combobox['filter_grism'], count_row, 1, 1, 2)
        count_row += 1

        layout.addWidget(self.label['exp_time'], count_row, 0)
        layout.addWidget(self.inputbox['exp_time'], count_row, 1)
        layout.addWidget(self.label['exp_unit'], count_row, 2)
        count_row += 1

        layout.addWidget(self.label['exp_multi'], count_row, 0)
        layout.addWidget(self.inputbox['exp_multi'], count_row, 1)
        layout.addWidget(self.label['times'], count_row, 2)
        count_row += 1

        if const_var['no_wipe']:
            layout.addWidget(self.label['no_wipe'], count_row, 0)
            layout.addWidget(self.button['no_wipe'], count_row, 1, 1, 2)
            count_row += 1

        layout.addWidget(self.button['start_exp_sequence'], count_row, 0, 1, 3)
        count_row += 1

        layout.addWidget(self.button['stop_all'], count_row, 0, 1, 3)
        count_row += 1

        layout.addWidget(self.label['message'], count_row, 0)
        count_row += 1

        layout.addWidget(self.textbox, count_row, 0, 1, 3)

        self.setLayout(layout)

# function
    def startMessia(self):
        common_func.socket_connect(['append_job StartMessia'])

    def stopMessia(self):
        common_func.socket_connect(['append_job StopMessia'])

    def update_grism_list(self):
        grism_list = common_func.get_grism_list()

        self.combobox['filter_grism'].clear()
        for tmp_item in grism_list:
            self.combobox['filter_grism'].addItem(tmp_item)

        return

    def toggle_no_wipe(self, checked):
        if checked:
            self.button['no_wipe'].setText('On')
        else:
            self.button['no_wipe'].setText('Off')

        return

    def startExposureSequence(self):
        filter_grism = self.combobox['filter_grism'].currentText()

        if self.inputbox['exp_time'].text() == '':
            exp_time_set = 0
        else:
            try:
                exp_time_set = int(self.inputbox['exp_time'].text())
            except:
                exp_time_set = 0

        if self.inputbox['exp_multi'].text() == '':
            exp_multi = 1
        else:
            try:
                exp_multi = int(self.inputbox['exp_multi'].text())
            except:
                exp_multi = 1

        command_sub = 'ExposureSequence {} {}'.format(filter_grism, exp_time_set).split()
        if self.button['no_wipe'].isChecked():
            command_sub += ['No_wipe']

        common_func.add_insert_job(command_sub, multi=exp_multi)

#    common_func.socket_connect(['{} ExposureSequence {} {}'.format(common_func.add_insert_job_format(), filter_grism, exp_time_set)] * exp_multi)

        return

    def initAllMotor(self):
        command_sub = 'MoveMotor'
        for tmp_motor in const_var['motor_name']:
# do not use Wheel-B
            if tmp_motor == 'Wheel-B':
                continue
            command_sub += ' {} orig'.format(tmp_motor)

# move Camera by 200 pulse from the origin
        command_sub += ' Camera absolute 200'

#    common_func.add_insert_job(command_sub)
        common_func.socket_connect(['{} {}'.format(common_func.add_insert_job_format(), command_sub)])

        return

    def stopMotorExposure(self):
        common_func.socket_connect(['quick_command stop_motor'])
        time.sleep(0.2)
        common_func.socket_connect(['quick_command stop_exposure'])

    def update_boxes(self):
        message = const_var['message_messia'] + '\n\n' + const_var['message_motor'] + '\n\n' + const_var['message_exp']
        self.textbox.setText(message)

# reload the grism list till the first initialization
        if not const_var['init_grism_list']:
            self.update_grism_list()
