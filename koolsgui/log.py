#!/usr/bin/python3

### gui/log.py: GUI for the Log window

### import libraries
import datetime

from PyQt5.QtWidgets import (QFileDialog, QGridLayout, QPushButton, QTableWidget, QTableWidgetItem, QFrame)
from PyQt5.QtCore import Qt

# original libraries
from const_var import const_var

### set constants

## define observation log box
class Frame(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# define variables
        headerTable = ['Date', 'Start Time', 'KLS ID', 'Exp. Time [s]', 'Object', 'Wheel-A', 'Wheel-B', 'Grism', 'Camera']
        self.dataTable = []

        self.button = {}

# make widgets
        self.button['load_tonight'] = QPushButton('Load tonight log')
        self.button['load_tonight'].clicked.connect(self.load_tonight_log)
        self.button['load_past'] = QPushButton('Load past log')
        self.button['load_past'].clicked.connect(self.load_past_log)
        self.table = QTableWidget(0, 9)
        self.table.setHorizontalHeaderLabels(headerTable)
        self.table.setStyleSheet('QAbstractScrollArea {font-size: 12pt;}')
#    self.table.setStyleSheet('QAbstractScrollArea {font-size: 12pt;}, QHeaderView {font-size: 12pt;}')

# set the common settings
        [self.button[i].setFont(const_var['font']) for i in self.button]

# layout
        layout = QGridLayout()
        layout.addWidget(self.button['load_tonight'], 0, 0, 1, 3)
        layout.addWidget(self.button['load_past'], 0, 3, 1, 3)
        layout.addWidget(self.table, 1, 0, 3, 6)

        self.setLayout(layout)

# function
    def load_tonight_log(self):
#    cmd = 'date -u +"%Y%m%d"'
#    ret = subprocess.check_output(cmd, shell=True, universal_newlines=True)
#    ret = subprocess.run(['date', '-u', '+"%Y%m%d"'], check = True, stdout = subprocess.PIPE)
#    ret_str = (str(ret.stdout))[2:-3].strip('"')
#    ret = datetime.datetime.utcnow()
#    year = str(ret.year)
#    date = year + '{}{}'.format(str(ret.month).zfill(2), str(ret.day).zfill(2))
#    log_file = '/data/messia/log/{}/obs{}.log'.format(year, date)

        time_now = datetime.datetime.utcnow()
        file_dir = '/data/messia/fits/{}/{}/'.format(time_now.strftime('%Y'), time_now.strftime('%Y%m%d'))
        log_file = file_dir + 'obs{}.log'.format(time_now.strftime('%Y%m%d'))

        self.load_obslog_file(log_file)

    def load_past_log(self):
        file_filter = 'Log files(*.log);; All files(*)'
        file_name = QFileDialog.getOpenFileName(self, 'Open file', '/data/messia/fits/', file_filter)
        if 'obs' in file_name[0] and '.log' in file_name[0]:
            self.load_obslog_file(file_name[0])
        else:
            print('File open error.')

    def load_obslog_file(self, log_file):
        while self.table.rowCount() > 0:
            self.table.removeRow(0)
        self.dataTable = []

        try:
            fin = open(log_file, 'r')
            line = fin.readline()
            while line:
                ret = line.split()
                self.dataTable.insert(0, ret)
                line = fin.readline()
            fin.close()

            for count2 in range(len(self.dataTable)):
                self.table.insertRow(count2)
                for count1 in range(9):
                    item = QTableWidgetItem(str(self.dataTable[count2][count1]))
                    item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEditable)
                    item.setForeground(Qt.black)
                    self.table.setItem(count2, count1, item)
        except IOError:
            pass
