#!/usr/bin/python3
# coding: utf-8

### GUI/Motor.py: GUI for motor controls ###

### import libraries
from PyQt5.QtWidgets import (QGridLayout, QLabel, QLineEdit, QTextEdit, QPushButton, QComboBox, QFrame)
from PyQt5.QtCore import Qt

# original libraries
import common_func
from const_var import const_var

## define motor box class
class Frame(QFrame):
# define constants
    MOTOR_NAME_LIST = const_var['motor_name']

    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
        self.namePos = {}
        self.label = {}
        self.combobox = {}
        self.inputbox = {}
        self.button = {}

# set variables
        for tmp_motor in Frame.MOTOR_NAME_LIST:
            self.namePos[tmp_motor] = ['orig', 'absolute', 'relative']

# get motor position settings
#    self.getMotorPosConfig()

# make buttons and input/output boxes
        self.label_motor = QLabel('Motor')
        self.label_motor.setFont(const_var['font_large'])

        self.button['init_all_motor'] = QPushButton('Initialize all motors')
        self.button['init_all_motor'].setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button['init_all_motor'].clicked.connect(self.init_all_motor)

        for tmp_motor in Frame.MOTOR_NAME_LIST:
            self.label[tmp_motor] = QLabel(tmp_motor)
            self.combobox[tmp_motor] = QComboBox()
#      for tmp_name in self.namePos[tmp_motor]:
#        self.comboBox[tmp_motor].addItem(tmp_name)
#      comboWheelA.setStyleSheet('QWidget { background-color: #D0FFD0 }')
            self.inputbox[tmp_motor] = QLineEdit()
            self.inputbox[tmp_motor].setText('')
            self.inputbox[tmp_motor].setPlaceholderText('Input Pulse')
            self.inputbox[tmp_motor].setMaxLength(6)
#      self.inputPulseWheelA.setStyleSheet('QWidget { background-color: #FFFF00 }')
            self.button['move_' + tmp_motor] = QPushButton('Move')
            self.button['move_' + tmp_motor].clicked.connect(self.moveMotor)
            self.button['move_' + tmp_motor].setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.button['stop_all'] = QPushButton('Stop All Motors')
        self.button['stop_all'].clicked.connect(self.stopMotor)
        self.button['stop_all'].setStyleSheet('QWidget { background-color: #FF0000 }')

        self.label['message'] = QLabel('Message')
        self.textbox = QTextEdit()
        self.textbox.setFont(const_var['font'])
        self.textbox.setReadOnly(True)
        self.textbox.setStyleSheet('QWidget { border: 2px solid black }')

# set the common settings
        [self.label[i].setFont(const_var['font']) for i in self.label]
        [self.combobox[i].setFont(const_var['font']) for i in self.combobox]
        [self.button[i].setFont(const_var['font']) for i in self.button]
        [self.inputbox[i].setFont(const_var['font']) for i in self.inputbox]
        [self.inputbox[i].setAlignment(Qt.AlignCenter) for i in self.inputbox]
#        [self.inputbox[i].setStyleSheet('QWidget { border: 2px solid black }') for i in self.inputbox]

# layout
        layout = QGridLayout()
        layout.setSpacing(10)
        count_row = 0

        layout.addWidget(self.label_motor, count_row, 0)
        count_row += 1

        layout.addWidget(self.button['init_all_motor'], count_row, 0, 1, 4)
        count_row += 1

        for count, tmp_motor in enumerate(Frame.MOTOR_NAME_LIST):
            layout.addWidget(self.label[tmp_motor], count_row + count, 0)
            layout.addWidget(self.combobox[tmp_motor], count_row + count, 1)
            layout.addWidget(self.inputbox[tmp_motor], count_row + count, 2)
            layout.addWidget(self.button['move_' + tmp_motor], count_row + count, 3)
        count_row += len(Frame.MOTOR_NAME_LIST)

        layout.addWidget(self.button['stop_all'], count_row, 0, 1, 4)
        count_row += 1

        layout.addWidget(self.label['message'], count_row, 0)
        count_row += 1

        layout.addWidget(self.textbox, count_row, 0, 1, 4)

        self.setLayout(layout)

# function
    def init_all_motor(self):
        command_sub = 'MoveMotor'
        for tmp_motor in Frame.MOTOR_NAME_LIST:
# do not use Wheel-B
            if tmp_motor == 'Wheel-B':
                continue
            command_sub += ' {} orig'.format(tmp_motor)

        common_func.socket_connect(['{} {}'.format(common_func.add_insert_job_format(), command_sub)])

        return

#  def getMotorPosConfig(self):
#    if const_var.socket_disable:
#      return
#
#    common_func.socket_connect(['exec_command get_motor_config'])
#
#    socket_cmd = ['get_status pos_list_{}'.format(i) for i in const_var.motor_name]
#    ret = common_func.socket_connect(socket_cmd)
#
#    for tmp_motor, tmp_ret in zip(const_var.motor_name, ret):
#      self.namePos[tmp_motor].extend(tmp_ret.split()[1::2])
#
#    return

    def update_motor_list(self):
        if const_var['socket_disable']:
            return

        common_func.socket_connect(['exec_command get_motor_config'])

        socket_cmd = ['get_status pos_list_{}'.format(i) for i in Frame.MOTOR_NAME_LIST]
        ret = common_func.socket_connect(socket_cmd)

        for tmp_motor, tmp_ret in zip(Frame.MOTOR_NAME_LIST, ret):
# initialize
            self.combobox[tmp_motor].clear()

            if tmp_motor == 'Wheel-A':
                self.namePos[tmp_motor] = ['orig', 'absolute', 'relative']
                if len(tmp_ret) > 1:
                    self.namePos[tmp_motor].extend(tmp_ret.split()[1::2])

            elif tmp_motor == 'Grism':
                self.namePos['Grism'] = ['orig', 'absolute', 'relative']
                self.namePos['Camera'] = ['orig', 'absolute', 'relative']
                if len(tmp_ret) > 1:
                    self.namePos['Grism'].extend(tmp_ret.split()[1::2])
                    self.namePos['Camera'].extend(tmp_ret.split()[1::2])

                if const_var['vph_blue_uv_ha'] and 'VPH-blue' in tmp_ret.split():
                    self.namePos['Camera'].extend(['VPH-blue-UV', 'VPH-blue-Ha'])

            for tmp_name in self.namePos[tmp_motor]:
                self.combobox[tmp_motor].addItem(tmp_name)

        const_var['init_motor_list'] = True

        return

    def moveMotor(self):
        button = self.sender()
        for tmp_motor in Frame.MOTOR_NAME_LIST:
            if button is self.button['move_' + tmp_motor]:
# do not use Wheel-b
#        if tmp_motor == 'Wheel-B':
#          continue

                motor = tmp_motor
                break

        else:
            print('Input is invalid.')
            return

        position = self.namePos[tmp_motor][self.combobox[tmp_motor].currentIndex()]

        pulse = self.inputbox[tmp_motor].text()
        if pulse == '':
            pulse = 0
        else:
            try:
                pulse = int(pulse)
            except:
                pulse = 0

        if position == 'absolute' or position == 'relative':
            command_sub = ['MoveMotor {} {} {}'.format(motor, position, pulse)]
        else:
            command_sub = ['MoveMotor {} {}'.format(motor, position)]

        common_func.add_insert_job(command_sub)

        return

    def stopMotor(self):
        common_func.socket_connect(['exec_command stop_motor'])
        return

    def update_boxes(self):
        self.textbox.setText(const_var['message_motor'])

# reload the grism list till the first initialization
        if not const_var['init_motor_list']:
            self.update_motor_list()

        return
