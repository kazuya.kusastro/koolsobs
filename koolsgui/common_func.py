#!/usr/bin/python3
# coding: utf-8

### common_func.py: define common functions

### import libraries
import socket

# original libraries
from const_var import const_var
from socket_client import socket_local, socket_global

### define constants
# socket connection
host = '192.168.1.64'
port = 50007
#delimiter = '!"#$%'
delimiter = '!DELIMITER!'
#message_end = 'END'
message_end = '!MESSAGE_END!'

### define functions
## GUI font setting
#def set_font_size():
#  if const_var.gui_font_size == 'small':
#    const_var.fontLabel.setPixelSize(24)
#    const_var.font.setPixelSize(16)
#    const_var.fontToolTip.setPixelSize(14)
#
#  else:
#    const_var.fontLabel.setPixelSize(28)
#    const_var.font.setPixelSize(20)
#    const_var.fontToolTip.setPixelSize(16)
#
##  koolsobs.app.exec_()
#  return

## socket connection
def socket_connect(input_list):
    if const_var['socket_disable']:
        return []
    elif const_var['socket_global']:
        return socket_global(input_list)
    else:
        return socket_local(input_list)

def get_status_single(key):
    ret = socket_connect(['get_status {}'.format(key)])
    ret_split = ret[0].split(' ')

    if len(ret_split) >= 2:
        return ret_split[1]
    else:
        return 'nop'

def add_insert_job(command_sub, multi=1):
    if const_var['append_insert_job'] == 'Insert':
        socket_connect(['insert_job {} {}'.format(const_var['job_current_id'] + 1, ' '.join(command_sub))] * multi)
    else:
        socket_connect(['append_job {}'.format(' '.join(command_sub))] * multi)

def add_insert_job_format():
    if const_var['append_insert_job'] == 'Insert':
        return 'insert_job {}'.format(const_var['job_current_id'] + 1)
    else:
        return 'append_job'

def get_kools_status():
# get the all status
    ret = socket_connect(['get_status all'])

# socket connection error
    if 'socket connection error' in ret[0]:
        const_var['socket_connection'] = False
#        const_var['job_now'] = 'socket disable'
        return
    else:
        const_var['socket_connection'] = True
# read the message
    keys = const_var.keys()
    for tmp_ret in ret:
        tmp_ret_split = tmp_ret.split()
        if len(tmp_ret_split) > 1 and tmp_ret_split[0] in keys:
            const_var[tmp_ret_split[0]] = ' '.join(tmp_ret_split[1:])

# get job list
    const_var['job_now'] = socket_connect(['get_job now'])[0]
    const_var['job_list'] = socket_connect(['get_job all'])

## socket connection error
#    if len(ret) == 0:
#        return
#
## read the message
#    for tmp_ret in ret:
#        tmp_ret_split = tmp_ret.split()
#
#        if len(tmp_ret_split) == 1:
#            tmp_ret_split.append('nop')
#
## messia
#        if tmp_ret_split[0] == 'status_messia':
#            const_var['status_messia'] = tmp_ret_split[1]
#        elif tmp_ret_split[0] == 'message_messia' and len(tmp_ret_split) >= 2:
#            const_var['message_messia'] = ' '.join(tmp_ret_split[1:])
#
## motor
#        elif 'pos_' in tmp_ret_split[0]:
#            tmp_str = tmp_ret_split[0].lstrip('pos_')
#            for tmp_motor in const_var['motor_name']:
#                if tmp_str == tmp_motor:
#                    const_var.motorPosition[tmp_motor] = tmp_ret_split[1]
#        elif 'pulse_' in tmp_ret_split[0]:
#            tmp_str = tmp_ret_split[0].lstrip('pulse_')
#            for tmp_motor in const_var.motor_name:
#                if tmp_str == tmp_motor:
#                    const_var.motorPulse[tmp_motor] = tmp_ret_split[1]
#
#        elif tmp_ret_split[0] == 'message_motor':
#            const_var.message_motor = ' '.join(tmp_ret_split[1:])
#
## exposure
#        elif tmp_ret_split[0] == 'ccd_file_number':
#            const_var.ccd_file_number = tmp_ret_split[1].zfill(8)
#        elif tmp_ret_split[0] == 'local_exp_str':
#            const_var.exp_start_time = tmp_ret_split[1]
#        elif tmp_ret_split[0] == 'temp_ccd_now':
#            const_var.ccd_temp_now = str(convert_to_float(tmp_ret_split[1], digit=3))
#        elif tmp_ret_split[0] == 'temp_heater_now':
#            const_var.ccd_heater_now = str(convert_to_float(tmp_ret_split[1], digit=1))
#        elif tmp_ret_split[0] == 'message_exp':
#            const_var.message_exp = ' '.join(tmp_ret_split[1:])
#
## stage position
#        elif tmp_ret_split[0] == 'signal_lamp':
#            const_var.signal_lamp = tmp_ret_split[1]
#        elif tmp_ret_split[0] == 'signal_tel':
#            const_var.signal_tel = tmp_ret_split[1]
#        elif tmp_ret_split[0] == 'signal_exp':
#            const_var.signal_exp = tmp_ret_split[1]
#
## misc
#        elif tmp_ret_split[0] == 'status_shutter':
#            const_var.status_shutter = tmp_ret_split[1]
#
## job
#        elif tmp_ret_split[0] == 'status_job':
#            const_var.status_job = tmp_ret_split[1]
#        elif tmp_ret_split[0] == 'job_now':
#            const_var.job_now = ' '.join(tmp_ret_split[1:])
#
## quicklook
#        elif tmp_ret_split[0] == 'ql_latest':
#            const_var.ql_latest = int(tmp_ret_split[1])
#
## sound
#        elif tmp_ret_split[0] == 'sound_exp':
#            const_var.sound_exp = tmp_ret_split[1]
#
## get job list
#    const_var.job_list = socket_connect(['get_job all'])

def get_grism_list():
    if const_var['socket_disable']:
        return []

    socket_connect(['exec_command get_motor_config'])

    ret = socket_connect(['get_status pos_list_Grism'])
    if len(ret) == 0:
        return []

    ret_split = ret[0].split(' ')
    del ret_split[0]

    grism_fil_list = []
    while len(ret_split) >= 2:
        if ret_split[0] == 'None':
            grism_fil_list.extend(['None'])
        elif ret_split[0] == 'VPH-blue':
            grism_fil_list.extend(['VPH-blue'])
            if const_var['vph_blue_uv_ha']:
                grism_fil_list.extend(['VPH-blue-UV', 'VPH-blue-Ha'])
        elif ret_split[0] == 'VPH-red':
            grism_fil_list.extend(['VPH-red_O56', 'VPH-red_no_O56'])
        elif ret_split[0] == 'VPH495':
            grism_fil_list.extend(['VPH495'])
        elif ret_split[0] == 'VPH683':
            grism_fil_list.extend(['VPH683_O56', 'VPH683_no_O56'])

        del ret_split[0:2]

    if len(grism_fil_list) > 0:
        const_var['init_grism_list'] = True
    else:
        print('Cannot get grism list. Try again.')

    return grism_fil_list

def convert_to_int(input_value):
    try:
        return int(input_value)
    except:
        return 'nop'

def convert_to_float(input_value, digit=6):
    try:
        return round(float(input_value), digit)
    except:
        return 'nop'
