#!/usr/bin/python3
# coding: utf-8

### gui/exposure.py: GUI for exposure commands ###

### import libraries
import subprocess

from PyQt5.QtWidgets import (QGridLayout, QLabel, QLineEdit, QTextEdit, QPushButton, QComboBox, QCheckBox, QFrame)
from PyQt5.QtCore import Qt

# original libraries
import common_func
from const_var import const_var

### set constants

## define exposure box
class Frame(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# set variables
        self.nameFilterGrism = []

        self.label = {}
        self.inputbox = {}
        self.combobox = {}
        self.button = {}
        self.checkbox = {}

# make buttons and input/output boxes
        self.label_exposure = QLabel('Exposure')
        self.label_exposure.setFont(const_var['font_large'])

        self.label['messia'] = QLabel('MESSIA')
        self.button['start_messia'] = QPushButton('Start')
        self.button['start_messia'].clicked.connect(self.startMessia)
        self.button['start_messia'].setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button['stop_messia'] = QPushButton('Stop')
        self.button['stop_messia'].clicked.connect(self.stopMessia)
        self.button['stop_messia'].setStyleSheet('QWidget { background-color: #FFB0B0 }')

        self.label['exp_time'] = QLabel('Exp./Sleep time')
        self.inputbox['exp_time'] = QLineEdit()
        self.inputbox['exp_time'].setText('')
#    self.boxExpTime.setStyleSheet('QWidget { background-color: #FFFFE8 }')
        self.inputbox['exp_time'].setMaxLength(5)
#    self.boxExpTime.setFixedWidth(90)
        self.inputbox['exp_time'].setPlaceholderText('Input Exp. Time')
        self.label['exp_unit'] = QLabel('sec.')

        self.label['filter_grism'] = QLabel('Filter and Grism')
        self.combobox['filter_grism'] = QComboBox()

        self.button['exposure'] = QPushButton('Exposure')
        self.button['exposure'].setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button['exposure'].clicked.connect(self.startExpSleep)
        self.button['exp_sequence'] = QPushButton('Exp. sequence')
        self.button['exp_sequence'].setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button['exp_sequence'].clicked.connect(self.startExposureSequence)
        self.button['sleep'] = QPushButton('Sleep')
        self.button['sleep'].clicked.connect(self.startExpSleep)

        self.button['stop_exposure'] = QPushButton('Stop exposure and sleep')
        self.button['stop_exposure'].setStyleSheet('QWidget { background-color: #FFB0B0 }')
        self.button['stop_exposure'].clicked.connect(self.stopExposure)

        self.checkbox['no_wipe'] = QCheckBox('No wipe')
        self.checkbox['no_tel_check'] = QCheckBox('No tel. check')

        self.label['ccd_temp'] = QLabel('CCD Temp.')
        self.button['update_ccd_temp'] = QPushButton('Update')
        self.button['update_ccd_temp'].clicked.connect(self.update_ccd_temp)

        self.label['shutter'] = QLabel('Shutter')
        self.button['open_shutter'] = QPushButton('Open')
#    self.buttonOpenShutter.setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button['open_shutter'].clicked.connect(self.controlShutter)
        self.button['close_shutter'] = QPushButton('Close')
#    self.buttonCloseShutter.setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button['close_shutter'].clicked.connect(self.controlShutter)

        self.label['message'] = QLabel('Message')
        self.textbox = QTextEdit()
        self.textbox.setFont(const_var['font'])
        self.textbox.setReadOnly(True)
        self.textbox.setStyleSheet('QWidget { border: 2px solid black }')

# set the common settings
        [self.label[i].setFont(const_var['font']) for i in self.label]
        [self.combobox[i].setFont(const_var['font']) for i in self.combobox]
        [self.button[i].setFont(const_var['font']) for i in self.button]
        [self.inputbox[i].setFont(const_var['font']) for i in self.inputbox]
#        [self.inputbox[i].setReadOnly(True) for i in self.inputbox]
        [self.inputbox[i].setAlignment(Qt.AlignCenter) for i in self.inputbox]
#        [self.inputbox[i].setStyleSheet('QWidget { border: 2px solid black }') for i in self.inputbox]
        [self.checkbox[i].setFont(const_var['font']) for i in self.checkbox]

# layout
        layout = QGridLayout()
        layout.setSpacing(10)
        count_row = 0

        layout.addWidget(self.label_exposure, count_row, 0)
        count_row += 1

        layout.addWidget(self.label['messia'], count_row, 0)
        layout.addWidget(self.button['start_messia'], count_row, 1)
        layout.addWidget(self.button['stop_messia'], count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(self.label['exp_time'], count_row, 0)
        layout.addWidget(self.inputbox['exp_time'], count_row, 1)
        layout.addWidget(self.label['exp_unit'], count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(self.label['filter_grism'], count_row, 0)
        layout.addWidget(self.combobox['filter_grism'], count_row, 1, 1, 3)
        count_row += 1

        layout.addWidget(self.button['exposure'], count_row, 0)
        layout.addWidget(self.button['exp_sequence'], count_row, 1)
        layout.addWidget(self.button['sleep'], count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(self.button['stop_exposure'], count_row, 0, 1, 4)
        count_row += 1

#        layout.addWidget(self.label['no_wipe_tel'], count_row, 0, 1, 2)
        layout.addWidget(self.checkbox['no_wipe'], count_row, 1)
        layout.addWidget(self.checkbox['no_tel_check'], count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(self.label['ccd_temp'], count_row, 0, 1, 2)
        layout.addWidget(self.button['update_ccd_temp'], count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(self.label['shutter'], count_row, 0)
        layout.addWidget(self.button['open_shutter'], count_row, 1)
        layout.addWidget(self.button['close_shutter'], count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(self.label['message'], count_row, 0)
        count_row += 1

        layout.addWidget(self.textbox, count_row, 0, 1, 4)

        self.setLayout(layout)

### functions
    def update_grism_list(self):
        grism_list = common_func.get_grism_list()

        self.combobox['filter_grism'].clear()
        [self.combobox['filter_grism'].addItem(i) for i in grism_list]

        return

    def startMessia(self):
        common_func.add_insert_job(['StartMessia'])
        return

    def stopMessia(self):
        common_func.add_insert_job(['StopMessia'])
        return

    def startExpSleep(self):
        if self.inputbox['exp_time'].text() == '':
            exp_time_set = 0
        else:
            exp_time_set = int(self.inputbox['exp_time'].text())

        sender = self.sender()
        if sender == self.button['exposure']:
            command_sub = ['Exposure', str(exp_time_set)]
            if self.checkbox['no_wipe'].checkState():
                command_sub += ['No_wipe']

        elif sender == self.button['sleep']:
            command_sub = ['Sleep', str(exp_time_set)]
        else:
            print('Input is invalid.')
            return 1

        common_func.add_insert_job(command_sub)
        return

    def startExposureSequence(self):
        filter_grism = self.combobox['filter_grism'].currentText()

        if self.inputbox['exp_time'].text() == '':
            exp_time_set = 0
        else:
            try:
                exp_time_set = int(self.inputbox['exp_time'].text())
            except:
                exp_time_set = 0

#    common_func.socket_connect(['{} ExposureSequence {} {}'.format(common_func.add_insert_job_format(), filter_grism, exp_time_set)])
        command_sub = 'ExposureSequence {} {}'.format(filter_grism, exp_time_set).split()
        if self.checkbox['no_wipe'].checkState():
            command_sub += ['No_wipe']
        if self.checkbox['no_tel_check'].checkState():
            command_sub += ['No_check']

        common_func.add_insert_job(command_sub)
        return

    def stopExposure(self):
        common_func.socket_connect(['quick_command stop_exposure'])
        return

    def update_ccd_temp(self):
        common_func.socket_connect(['quick_command get_ccd_temp'])
        return

    def controlShutter(self):
        sender = self.sender()
        if sender == self.button['open_shutter']:
            command_sub = ['Shutter', 'open']
        elif sender == self.button['close_shutter']:
            command_sub = ['Shutter', 'close']
        else:
            return

        common_func.add_insert_job(command_sub)

        return

    def update_boxes(self):
        message_exp = const_var['message_messia'] + '\n\n' + const_var['message_exp']
        self.textbox.setText(message_exp)

# reload the grism list till the first initialization
        if not const_var['init_grism_list']:
            self.update_grism_list()

        return
