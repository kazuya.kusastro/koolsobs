#!/usr/bin/python3

### gui/Status.py: GUI for displaying KOOLS status ###

### import libraries
import subprocess

from PyQt5.QtWidgets import (QGridLayout, QLabel, QLineEdit, QFrame)
from PyQt5.QtCore import Qt

# original libraries
from const_var import const_var

### set constants

## define exposure box
class Frame(QFrame):
# define constants
    MOTOR_NAME_LIST = const_var['motor_name']

    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
        self.label = {}
        self.inputbox = {}

        self.motorPosition = {}
        self.motorPulse = {}

# define buttons
        self.label_status = QLabel('Status')
        self.label_status.setFont(const_var['font_large'])

        self.label['messia'] = QLabel('Messia')
        self.inputbox['messia'] = QLineEdit()
        self.inputbox['messia'].setStyleSheet('QWidget { border: 2px solid black }')

        self.label['grid_line1'] = QLabel('-' * 30)
        self.label['grid_line1'].setAlignment(Qt.AlignCenter)

        self.label['motor'] = QLabel('Motor')
        self.label['position'] = QLabel('Position')
        self.label['position'].setAlignment(Qt.AlignCenter)
        self.label['pulse'] = QLabel('Pulse')
        self.label['pulse'].setAlignment(Qt.AlignCenter)

        for tmp_motor in Frame.MOTOR_NAME_LIST:
            self.label[tmp_motor] = QLabel(tmp_motor)
            self.inputbox['pos_' + tmp_motor] = QLineEdit()
            self.inputbox['pulse_' + tmp_motor] = QLineEdit()

        self.label['grid_line2'] = QLabel('-' * 30)
        self.label['grid_line2'].setAlignment(Qt.AlignCenter)

        self.label['frame_id'] = QLabel('Frame ID')
        self.inputbox['frame_id'] = QLineEdit()
        self.label['start_time'] = QLabel('Start time')
        self.inputbox['start_time'] = QLineEdit()

        self.label['ccd_temp'] = QLabel('CCD Temp.')
        self.inputbox['ccd_temp'] = QLineEdit()
        self.label['deg_C'] = QLabel('deg. C')
        self.label['ccd_heater'] = QLabel('CCD Heater Power')
        self.inputbox['ccd_heater'] = QLineEdit()
        self.label['parsent'] = QLabel('%')

        self.label['shutter'] = QLabel('Shutter')
        self.inputbox['shutter'] = QLineEdit()

# set the common settings
        [self.label[i].setFont(const_var['font']) for i in self.label]
        [self.inputbox[i].setFont(const_var['font']) for i in self.inputbox]
        [self.inputbox[i].setReadOnly(True) for i in self.inputbox]
        [self.inputbox[i].setAlignment(Qt.AlignCenter) for i in self.inputbox]
        [self.inputbox[i].setStyleSheet('QWidget { border: 2px solid black }') for i in self.inputbox]

# layout
        layout = QGridLayout()
        layout.setSpacing(10)
        count_row = 0

        layout.addWidget(self.label_status, count_row, 0)
        count_row += 1

        layout.addWidget(self.label['messia'], count_row, 0, 1, 2)
        layout.addWidget(self.inputbox['messia'], count_row, 2)
        count_row += 1

        layout.addWidget(self.label['grid_line1'], count_row, 0, 1, 3)
        count_row += 1

        layout.addWidget(self.label['motor'], count_row, 0)
        layout.addWidget(self.label['position'], count_row, 1)
        layout.addWidget(self.label['pulse'], count_row, 2)
        count_row += 1

        for count, tmp_motor in enumerate(Frame.MOTOR_NAME_LIST):
            layout.addWidget(self.label[tmp_motor], count_row + count, 0)
            layout.addWidget(self.inputbox['pulse_' + tmp_motor], count_row + count, 1)
            layout.addWidget(self.inputbox['pos_' + tmp_motor], count_row + count, 2)
        count_row += len(Frame.MOTOR_NAME_LIST)

        layout.addWidget(self.label['grid_line2'], count_row, 0, 1, 3)
        count_row += 1

        layout.addWidget(self.label['frame_id'], count_row, 0, 1, 2)
        layout.addWidget(self.inputbox['frame_id'], count_row, 2)
        count_row += 1

        layout.addWidget(self.label['start_time'], count_row, 0, 1, 2)
        layout.addWidget(self.inputbox['start_time'], count_row, 2)
        count_row += 1

        layout.addWidget(self.label['ccd_temp'], count_row, 0)
        layout.addWidget(self.inputbox['ccd_temp'], count_row, 1)
        layout.addWidget(self.label['deg_C'], count_row, 2)
        count_row += 1

        layout.addWidget(self.label['ccd_heater'], count_row, 0)
        layout.addWidget(self.inputbox['ccd_heater'], count_row, 1)
        layout.addWidget(self.label['parsent'], count_row, 2)
        count_row += 1

        layout.addWidget(self.label['shutter'], count_row, 0, 1, 2)
        layout.addWidget(self.inputbox['shutter'], count_row, 2)

        self.setLayout(layout)

# functions
    def set_motor_value(self, key, input_value):
        self.inputbox[key].setText(str(input_value))

        if input_value == 'moving':
            self.inputbox[key].setStyleSheet("QWidget { color: #0000FF ; border: 2px solid black }")
        elif input_value in ('unknown', 'error'):
            self.inputbox[key].setStyleSheet("QWidget { color: #FF0000 ; border: 2px solid black }")
        else:
            self.inputbox[key].setStyleSheet("QWidget { color: black ; border: 2px solid black }")

        return

    def setFontColor(self):
        for tmp_motor in Frame.MOTOR_NAME_LIST:
            if self.inputbox['motor_pulse_' + tmp_motor].text() == 'moving':
                self.motorPosition[tmp_motor].setStyleSheet("QWidget { color: #0000FF ; border: 2px solid black }")
            elif self.motorPosition[tmp_motor].text() == 'unknown' or self.motorPosition[tmp_motor].text() == 'error':
                self.motorPosition[tmp_motor].setStyleSheet("QWidget { color: #FF0000 ; border: 2px solid black }")
            else:
                self.motorPosition[tmp_motor].setStyleSheet("QWidget { color: black ; border: 2px solid black }")

            if self.motorPulse[tmp_motor].text() == 'moving':
                self.motorPulse[tmp_motor].setStyleSheet("QWidget { color: #0000FF ; border: 2px solid black }")
            elif self.motorPulse[tmp_motor].text() == 'error':
                self.motorPulse[tmp_motor].setStyleSheet("QWidget { color: #FF0000 ; border: 2px solid black }")
            else:
                self.motorPulse[tmp_motor].setStyleSheet("QWidget { color: black ; border: 2px solid black }")

    def update_boxes(self):
        self.inputbox['messia'].setText(const_var['status_messia'])

        for tmp_motor in Frame.MOTOR_NAME_LIST:
            tmp_split = const_var['pos_' + tmp_motor].split()
            if len(tmp_split) < 1:
                continue
            tmp_key = 'pos_' + tmp_motor
            self.set_motor_value(tmp_key, tmp_split[0])

            tmp_split = const_var['pulse_' + tmp_motor].split()
            if len(tmp_split) < 1:
                continue
            tmp_key = 'pulse_' + tmp_motor
            self.set_motor_value(tmp_key, tmp_split[0])

#            self.inputbox['motor_pulse_' + tmp_motor].setText(str(pulse))
#            self.inputbox['motor_pos_' + tmp_motor].setText(str(position))

#        for tmp_motor in const_var.motor_name:
#            self.motorPosition[tmp_motor].setText(const_var.motorPosition[tmp_motor])
#            self.motorPulse[tmp_motor].setText(const_var.motorPulse[tmp_motor])

#        self.setFontColor()

        self.inputbox['frame_id'].setText(str(const_var['ccd_file_number']))
        self.inputbox['start_time'].setText(const_var['local_exp_str'])
        self.inputbox['ccd_temp'].setText(const_var['temp_ccd_now'])
        self.inputbox['ccd_heater'].setText(const_var['temp_heater_now'])
        self.inputbox['shutter'].setText(str(const_var['status_shutter']))

        return

