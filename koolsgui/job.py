#!/usr/bin/python3

### gui/job.py: GUI for Job window

### import libraries
import subprocess

from PyQt5.QtWidgets import (QFileDialog, QGridLayout, QLabel, QLineEdit, QPushButton, QRadioButton, QButtonGroup, QFrame, QListWidget, QMessageBox)
from PyQt5.QtCore import Qt

# original libraries
from const_var import const_var
import common_func

### set constants

## define job box
class Frame(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
        self.label = {}
        self.inputbox = {}
        self.button = {}
        self.radio = {}

        self.job_list_old = []

# make buttons and input/output boxes
        label_job = QLabel('Job')
        label_job.setFont(const_var['font_large'])

        self.label['server'] = QLabel('Server')
        self.label['server'].setFixedWidth(100)
        self.inputbox['server'] = QLineEdit()
        self.inputbox['server'].setFixedWidth(120)
        self.inputbox['server'].setAlignment(Qt.AlignCenter)
#        self.inputbox['Server'].setText(const_var['status_job'])

        self.button['start_server'] = QPushButton('Start')
        self.button['start_server'].clicked.connect(self.start_server)
#    self.buttonStartServer.setFixedWidth(250)
        self.button['start_server'].setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button['start_server'].setEnabled(False)

        self.button['stop_server'] = QPushButton('Stop')
        self.button['stop_server'].clicked.connect(self.stop_server)
#    self.buttonStopServer.setFixedWidth(250)
        self.button['stop_server'].setStyleSheet('QWidget { background-color: #FFB0B0 }')
        self.button['stop_server'].setEnabled(True)

        self.label['append_insert'] = QLabel('append/insert')
        self.button['append_insert'] = QPushButton('Append')
        self.button['append_insert'].setCheckable(True)
        self.button['append_insert'].toggled.connect(self.toggle_append_insert)
        const_var['append_insert_job'] = self.button['append_insert'].text()

#    radioAppend = QRadioButton('Append')
#    radioAppend.setFont(const_var.font)
#    radioAppend.setChecked(True)
#    radioInsert = QRadioButton('Insert')
#    radioInsert.setFont(const_var.font)
#    radioInsert.setChecked(False)
#    self.groupAppendInsert = QButtonGroup()
#    self.groupAppendInsert.addButton(radioAppend, 1)
#    self.groupAppendInsert.addButton(radioInsert, 2)
#    radioAppend.clicked.connect(self.check_append_insert)
#    radioInsert.clicked.connect(self.check_append_insert)

        self.button['import_file'] = QPushButton('Import job file')
        self.button['import_file'].clicked.connect(self.import_job)
        self.button['delete_job'] = QPushButton('Delete')
        self.button['delete_job'].clicked.connect(self.delete_job)
#    self.buttonDelete.setFixedWidth(250)
        self.button['delete_job'].setStyleSheet('QWidget { background-color: #FFB0B0 }')
        self.button['delete_job_all'] = QPushButton('Delete All')
        self.button['delete_job_all'].clicked.connect(self.delete_all_job)
#    self.buttonDeleteAll.setFixedWidth(250)
        self.button['delete_job_all'].setStyleSheet('QWidget { background-color: #FF0000 }')

        self.label['sound'] = QLabel('Sound')
        self.radio['sound_on'] = QRadioButton('On')
        self.radio['sound_on'].setChecked(False)
        self.radio['sound_on'].clicked.connect(self.setSound)
        self.radio['sound_off'] = QRadioButton('Off')
        self.radio['sound_off'].setChecked(True)
        self.radio['sound_off'].clicked.connect(self.setSound)
        self.group_sound = QButtonGroup()
        self.group_sound.addButton(self.radio['sound_on'])
        self.group_sound.addButton(self.radio['sound_off'])

        self.label['now'] = QLabel('Now')
        self.label['now'].setAlignment(Qt.AlignRight)
        self.inputbox['job_now'] = QLineEdit()
        self.label['next'] = QLabel('Next')
        self.label['next'].setAlignment(Qt.AlignRight)
        self.job_list = QListWidget()
        self.job_list.setFont(const_var['font'])
        self.job_list.currentRowChanged.connect(self.check_list_row)
        self.job_list.setStyleSheet('QWidget { border: 2px solid black }')

# set the common settings
        [self.label[i].setFont(const_var['font']) for i in self.label]
        [self.button[i].setFont(const_var['font']) for i in self.button]
        [self.inputbox[i].setFont(const_var['font']) for i in self.inputbox]
        [self.inputbox[i].setReadOnly(True) for i in self.inputbox]
        [self.inputbox[i].setStyleSheet('QWidget { border: 2px solid black }') for i in self.inputbox]
        [self.radio[i].setFont(const_var['font']) for i in self.radio]

# layout
        layout = QGridLayout()
        layout.setSpacing(10)
        count_row = 0

        layout.addWidget(self.label['server'], count_row, 0, 1, 2)
        layout.addWidget(self.inputbox['server'], count_row, 2)
        count_row += 1

        layout.addWidget(self.button['start_server'], count_row, 0, 1, 2)
        layout.addWidget(self.button['stop_server'], count_row, 2)
        count_row += 1

        layout.addWidget(self.label['append_insert'], count_row, 0, 1, 2)
        layout.addWidget(self.button['append_insert'], count_row, 2)
        count_row += 1

        layout.addWidget(self.button['import_file'], count_row, 0, 1, 3)
        count_row += 1

        layout.addWidget(self.button['delete_job'], count_row, 0, 1, 2)
        layout.addWidget(self.button['delete_job_all'], count_row, 2)
        count_row += 1

        layout.addWidget(self.label['sound'], count_row, 0)
        layout.addWidget(self.radio['sound_on'], count_row, 1)
        layout.addWidget(self.radio['sound_off'], count_row, 2)

        count_row = 0

        layout.addWidget(self.label['now'], count_row, 3)
        layout.addWidget(self.inputbox['job_now'], count_row, 4, 1, 10)
        count_row += 1

        layout.addWidget(self.label['next'], count_row, 3)
        layout.addWidget(self.job_list, count_row, 4, 5, 10)

        self.setLayout(layout)

# function
    def check_server(self):
        if const_var['status_job'] == 'start':
            self.button['start_server'].setEnabled(False)
            self.button['stop_server'].setEnabled(True)
        elif const_var['status_job'] == 'stop':
            self.button['start_server'].setEnabled(True)
            self.button['stop_server'].setEnabled(False)
        else:
            self.button['start_server'].setEnabled(True)
            self.button['stop_server'].setEnabled(True)

    def show_list(self):
        if const_var['job_list'] != self.job_list_old:
            self.job_list_old = const_var['job_list']
            self.job_list.clear()
            for tmp_cmd in const_var['job_list']:
                self.job_list.addItem(tmp_cmd)
            if const_var['job_current_id'] < len(const_var['job_list']):
                self.job_list.setCurrentRow(const_var['job_current_id'])

    def start_server(self):
        common_func.socket_connect(['set_status status_job start', 'set_status job_now finish'])

    def stop_server(self):
        common_func.socket_connect(['set_status status_job stop'])

    def check_append_insert(self):
        sender = self.sender()
        const_var['append_insert_job'] = sender.text()

    def toggle_append_insert(self, checked):
        if checked:
            self.button['append_insert'].setText('Insert')
        else:
            self.button['append_insert'].setText('Append')

        const_var['append_insert_job'] = self.button['append_insert'].text()

        return

    def check_list_row(self, item):
        const_var['job_current_id'] = item

    def import_job(self):
        file_name = QFileDialog.getOpenFileName(self, 'Open file', './')
        if file_name[0]:
            with open(file_name[0], 'r') as fin:
                if const_var['append_insert_job'] == 'Insert':
                    for file_line in reversed(fin.readlines()):
                        common_func.socket_connect(['insert_job {} {}'.format(const_var['job_current_id'] + 1, file_line)])
                else:
                    for file_line in fin:
                        common_func.socket_connect(['append_job {}'.format(file_line)])
        else:
            print('File open error.')

    def delete_job(self):
        if self.job_list.currentItem() is not None:
            common_func.socket_connect(['delete_job {}'.format(self.job_list.currentRow() + 1)])

    def delete_all_job(self):
        msg_box = QMessageBox()
        msg_box.setIcon(QMessageBox.Question)
        msg_box.setWindowTitle('Delete all jobs')
        msg_box.setText('Do you want to delete all the jobs?')
        msg_box.setFont(const_var['font_tooltip'])
        msg_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        msg_box.setDefaultButton(QMessageBox.No)
        reply = msg_box.exec()

#    reply = QMessageBox.question(self, 'Message', 'Are you sure to delete ALL jobs?', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            common_func.socket_connect(['delete_job all'])

    def setSound(self):
        sender = self.sender()
        common_func.socket_connect(['set_status sound_exp {}'.format(sender.text())])

    def check_sound(self):
        if const_var['sound_exp'] == 'On':
            self.radio['sound_on'].setChecked(True)
            self.radio['sound_off'].setChecked(False)
        else:
            self.radio['sound_on'].setChecked(False)
            self.radio['sound_off'].setChecked(True)

    def update_boxes(self):
        self.check_server()
        self.show_list()
        self.check_sound()
        self.inputbox['server'].setText(const_var['status_job'])
        self.inputbox['job_now'].setText(const_var['job_now'])
